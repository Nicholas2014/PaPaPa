﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Web.Models.Accounts
{
    public class UserModel : IUser, IUserStore<UserModel>
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        #region IUserStore<UserModel> 成员

        public Task CreateAsync(UserModel user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(UserModel user)
        {
            throw new NotImplementedException();
        }

        public Task<UserModel> FindByIdAsync(string userId)
        {
            throw new NotImplementedException();
        }

        public Task<UserModel> FindByNameAsync(string userName)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(UserModel user)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDisposable 成员

        public void Dispose()
        {
            //noting to do
        }

        #endregion
    }
}
