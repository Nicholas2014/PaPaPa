﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Web.Common.Auths.QQ
{
    public static class QQAccountAuthenticationExtensions
    {
        public static IAppBuilder UseMicrosoftAccountAuthentication(this IAppBuilder app, QQAccountAuthenticationOptions options)
        {
            if (app == null)
            {
                throw new ArgumentNullException("app");
            }
            if (options == null)
            {
                throw new ArgumentNullException("options");
            }
            app.Use(typeof(QQAccountAuthenticationMiddleware), new object[] { app, options });
            return app;
        }
    }
}
