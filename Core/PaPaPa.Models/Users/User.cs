﻿using Framework.Mapping.Attributes;
using PaPaPa.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Models.Accounts
{
    public class User : EntityBase
    {
        [Key]
        public int Id { get; set; }

        [Required, MaxLength(20)]
        public string UserName { get; set; }

        [Required, MaxLength(20)]
        public string Password { get; set; }

        public string Email { get; set; }

        public string NickName { get; set; }

        public string ImgSrc { get; set; }

        public string MobilePhone { get; set; }

        public string Province { get; set; }

        public string City { get; set; }

        public string Area { get; set; }

        public User()
        {
            ImgSrc = "";
            Email = "";
            MobilePhone = "";
            NickName = "";
            Province = "";
            City = "";
            Area = "";
        }
    }
}
